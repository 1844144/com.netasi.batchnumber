<?php

require_once 'batchnumber.civix.php';

/**
 * Implementation of hook_civicrm_config
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function batchnumber_civicrm_config(&$config) {
  _batchnumber_civix_civicrm_config($config);
}

/**
 * Implementation of hook_civicrm_xmlMenu
 *
 * @param $files array(string)
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function batchnumber_civicrm_xmlMenu(&$files) {
  _batchnumber_civix_civicrm_xmlMenu($files);
}

/**
 * Implementation of hook_civicrm_install
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function batchnumber_civicrm_install() {
  _batchnumber_civix_civicrm_install();
}

/**
 * Implementation of hook_civicrm_uninstall
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function batchnumber_civicrm_uninstall() {
  _batchnumber_civix_civicrm_uninstall();
}

/**
 * Implementation of hook_civicrm_enable
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function batchnumber_civicrm_enable() {
  _batchnumber_civix_civicrm_enable();
}

/**
 * Implementation of hook_civicrm_disable
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function batchnumber_civicrm_disable() {
  _batchnumber_civix_civicrm_disable();
}

/**
 * Implementation of hook_civicrm_upgrade
 *
 * @param $op string, the type of operation being performed; 'check' or 'enqueue'
 * @param $queue CRM_Queue_Queue, (for 'enqueue') the modifiable list of pending up upgrade tasks
 *
 * @return mixed  based on op. for 'check', returns array(boolean) (TRUE if upgrades are pending)
 *                for 'enqueue', returns void
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function batchnumber_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _batchnumber_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implementation of hook_civicrm_managed
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function batchnumber_civicrm_managed(&$entities) {
  _batchnumber_civix_civicrm_managed($entities);
}

/**
 * Implementation of hook_civicrm_caseTypes
 *
 * Generate a list of case-types
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function batchnumber_civicrm_caseTypes(&$caseTypes) {
  _batchnumber_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implementation of hook_civicrm_alterSettingsFolders
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function batchnumber_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _batchnumber_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

function batchnumber_civicrm_preProcess($formName, &$form){

  if ($formName == 'CRM_Contribute_Form_ContributionView') {
    $id = $form->get('id');
    $values = $ids = array();
    $params = array('id' => $id);

    CRM_Contribute_BAO_Contribution::getValues($params, $values, $ids);

    $cid = $values['id'];

    $sql = " SELECT ba.id , ba.name from civicrm_batch ba 
                    left join civicrm_entity_batch eba on eba.batch_id=ba.id AND eba.entity_table='civicrm_financial_trxn'
                    left join civicrm_entity_financial_trxn etr on etr.financial_trxn_id=eba.entity_id AND etr.entity_table='civicrm_contribution'
                    
                    where etr.entity_id=$cid
                ";
    $dao = CRM_Core_DAO::executeQuery($sql);
    if ( $dao->fetch() ) {
      $form->assign('batch', array( 'id' => $dao->id, 'name'=> $dao->name ));

       $templatePath = realpath(dirname(__FILE__)."/templates");
        CRM_Core_Region::instance('page-body')->add(array(
          'template' => "{$templatePath}/field.tpl"
        ));
   
    }
  }
  
}

// 
// Add batch Number to Contribution Report

function batchnumber_civicrm_alterReportVar($varType, &$var, &$object) {
  
  // $instanceValue = $object->getVar('_instanceValues');
  $name = $object->getVar('_name');
  
  if ($name =='Detail'){
    if ($varType  == 'columns'){
      $var['civicrm_batch']['fields']['batch_orig'] =  array(
            'title' => 'Batch ID',
            'name' => 'batch_orig',
            'type' => 1,
            'dbAlias' => "batch_civireport.batch_id",
          );

    } else if ($varType == 'rows'){
      $var['civicrm_batch']['fields']['batch_orig']['title'] = 'Batch ID';
    }
  }
}







